import java.util.*;

public class Tnode {

   public static final String SWAP = "SWAP";
   public static final String ROT = "ROT";
   public static final List<String> stackOperators = Arrays.asList(SWAP, ROT);
   public static final List<String> operators = Arrays.asList("+", "-", "*", "/");

   private String name;
   private Tnode firstChild;
   private Tnode nextSibling;

   @Override
   public String toString() {
      StringBuffer b = new StringBuffer();
      if (Objects.nonNull(name)) {
         b.append(name);
         if (operators.contains(name)) {
            b.append("(");
            if (Objects.nonNull(firstChild)) {
               b.append(firstChild.toString());
            }
            b.append(",");
            if (Objects.nonNull(nextSibling)) {
               b.append(nextSibling.toString());
            }
            b.append(")");
         }
      }
      return b.toString();
   }

   public static Tnode buildFromRPN (String pol) {
      if (pol == null || "".equals(pol)) throw new RuntimeException("tyhi avaldis!");
      Tnode root = null;
      Tnode siblingRoot = null;
      Tnode firstChild = null;
      Tnode nextSibling = null;
      Tnode first;
      Tnode second;
      Tnode third;

      String[] input = pol.split(" ");
      if (input.length == 1) {
          if (isNumeric(pol)) {
            root = new Tnode();
            root.name = pol;
            return root;
          } else if (operators.contains(pol)) {
             throw new RuntimeException("liiga v2he numbreid avaldises: " + pol);
          } else throw new RuntimeException("avaldiseks on ainult tundmatu symbol: " + pol);
      }

      for (String currentElement : input) {
         if (isNumeric(currentElement)) {
            Tnode node = new Tnode();
            node.name = currentElement;
            if (Objects.isNull(firstChild)) {
               firstChild = node;
            } else if (Objects.isNull(nextSibling)) {
               nextSibling = node;
            } else {
               siblingRoot = node;
            }
         } else if (stackOperators.contains(currentElement)) {
            switch (currentElement) {
               case SWAP:
                  first = firstChild;
                  second = nextSibling;
                  firstChild = second;
                  nextSibling = first;
                  break;
               case ROT:
                  first = firstChild;
                  second = nextSibling;
                  assert nextSibling != null;
                  third = nextSibling.firstChild;
                  nextSibling.firstChild = null;
                  firstChild = second;
                  nextSibling = first;
                  assert nextSibling != null;
                  nextSibling.nextSibling = third;
                  break;
            }
         } else if (operators.contains(currentElement)) {
            if (Objects.isNull(root) && Objects.nonNull(firstChild) && Objects.nonNull(nextSibling) && Objects.nonNull(siblingRoot)) {
               second = nextSibling;
               nextSibling = new Tnode();
               nextSibling.name = currentElement;
               nextSibling.firstChild = siblingRoot;
               nextSibling.nextSibling = second;
               siblingRoot = null;
            } else
            if (Objects.isNull(root)) {
               root = new Tnode();
               root.name = currentElement;
               if (Objects.nonNull(firstChild)) {
                  root.firstChild = firstChild;
                  firstChild = null;
               }
               if (Objects.nonNull(nextSibling)) {
                  root.nextSibling = nextSibling;
                  nextSibling = null;
               }
            } else if (Objects.isNull(firstChild) && Objects.isNull(nextSibling) && Objects.isNull(siblingRoot)) {
               if (root.name != null) {
                  throw new RuntimeException("liiga v2he numbreid avaldises " + pol);
               }
               root.name = currentElement;
            }
            else {
               siblingRoot = root;
               root = new Tnode();
               if (Objects.nonNull(firstChild) && Objects.isNull(nextSibling)) {
                  root.name = currentElement;
                  root.firstChild = siblingRoot;
                  root.nextSibling = firstChild;
                  firstChild = null;
               } else if (Objects.nonNull(firstChild)) {
                  root.firstChild = siblingRoot;
                  Tnode rightNode = new Tnode();
                  rightNode.name = currentElement;
                  rightNode.firstChild = firstChild;
                  rightNode.nextSibling = nextSibling;
                  root.nextSibling = rightNode;
                  firstChild = null;
                  nextSibling = null;
               } else {
                  root = siblingRoot;
                  root.name = currentElement;
               }
            }
         } else throw new RuntimeException("tundmatu symbol " + currentElement + " avaldises: " + pol);
      }

      if (Objects.isNull(root) && Objects.nonNull(firstChild) && Objects.nonNull(nextSibling) ||
          Objects.nonNull(root) && Objects.nonNull(firstChild) && Objects.isNull(nextSibling)) {
         throw new RuntimeException("liiga palju numbreid avaldises " + pol);
      }

      if (Objects.nonNull(root) && Objects.nonNull(root.firstChild) && Objects.isNull(root.nextSibling)) {
         throw new RuntimeException("liiga v2he numbreid avaldises " + pol);
      }

      return root;
   }

   private static boolean isNumeric(String s) {
      if (s == null) return false;

      try {
         int i = Integer.parseInt(s);
      } catch (NumberFormatException nfe) {
         return false;
      }

      return true;
   }

   public static void main (String[] param) { }
}

